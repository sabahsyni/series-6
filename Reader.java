package sbu.cs.exception;

import java.util.Arrays;
import java.util.List;

public class Reader {

    /**
     * declare 2 Exception class. 1 for UnrecognizedCommand and 1 for NotImplementedCommand
     * iterate on function inputs and check for commands and throw exception when needed.
     *
     * @param args
     */
    public  void readTwitterCommands(List<String> args) throws Exception {
        for (int i=0; i< args.size();i++)
        {
            if ((Util.getNotImplementedCommands().contains(args.get(i)))) {
                new ApException(1);
            }
            else if (!(Util.getImplementedCommands().contains(args.get(i))))
            {
                new ApException(1,1);
            }
        }
    }

    /**
     * function inputs are String but odd positions must be integer parsable
     * a valid input is like -> "ap", "2", "beheshti", "3992", "20"
     * throw BadInput exception when the string is not parsable.
     *
     * @param args
     */
    public  void read(String...args) throws Exception {
        for (int i=1;i< args.length;i+=2)
        {
            try {
                int n= Integer.parseInt(args[i]);
            }
            catch (Exception exception)
            {
                new ApException();
            }
        }

    }
}
