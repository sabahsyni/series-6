package sbu.cs.exception;

public class ApException {
    public ApException(int a) throws Exception {
        throw new Exception("NotImplementedCommand");
    }
    public ApException(int a, int b) throws Exception
    {
        throw new Exception("UnrecognizedCommand");
    }
    public ApException() throws Exception
    {
        throw new Exception("BadInput");
    }
}

